USE [CLEANSING]
GO

/****** Object:  StoredProcedure [dbo].[usp_rp_masivo_corporativo]    Script Date: 25/5/2022 15:25:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_rp_masivo_corporativo] 
@IdTipoRequerimiento INT,
@fechaInicio VARCHAR(50),
@fechaFin VARCHAR(50)
AS
BEGIN

/*
	EXEC usp_rp_masivo_corporativo  '35', '2022-03-10 00:00:00', '2022-03-10 09:00:00'
	DROP PROCEDURE usp_rp_masivo_corporativo
*/

DECLARE @ID_TICKET INT;
DECLARE @CODIGO_ORIGEN VARCHAR(100);
DECLARE @FECHA_REQUERIMIENTO VARCHAR(1000);
DECLARE @PROCESS_INTANSCE VARCHAR(1000);
DECLARE @REQUERIMIENTO VARCHAR(1000);
DECLARE @IDENTIFICADOR_TECNICO VARCHAR(1000);
DECLARE @NOMBRE_VALOR VARCHAR(1000);
DECLARE @VALOR VARCHAR(1000);
DECLARE @FECHA_CONCRETADO VARCHAR(1000);

IF (OBJECT_ID('#TempRpMasivoCorporativo') > 0)
	BEGIN
		DELETE FROM #TempRpMasivoCorporativo;
	END
ELSE
	BEGIN
	CREATE TABLE #TempRpMasivoCorporativo
	(
		[idTicket] INT,
		[codigoOrigen] VARCHAR(100),
		[identificadorTecnico] VARCHAR(1000),
		[fechaRegistro] VARCHAR(1000),
		[diaRegistro] VARCHAR(1000),
		[mesRegistro] VARCHAR(1000),
		[anioRegistro] VARCHAR(1000),
		[ultimaMilla] VARCHAR(1000),
		[lineaNegocio] VARCHAR(1000),
		[diaConcretado] VARCHAR(1000),
		[mesConcretado] VARCHAR(1000),
		[anioConcretado] VARCHAR(1000),
		[tareaActualBpm] VARCHAR(1000),
		[requerimiento] VARCHAR(1000),
		[fechaPrimeraFactura] VARCHAR(1000),
		[Motivo Generacion Visita] VARCHAR(1000),
	)
	END

	/*IF(@IdTipoRequerimiento = 33)
		SET @IdTipoRequerimiento = (SELECT CONCAT(@IdTipoRequerimiento,',',34))*/
	
SELECT t.id,t.name,t.activationtime,pap.entity_id,pap.task_id,t.status,t.processinstanceid, deploymentid
INTO #reservados
from CLEANSING.dbo.fnt_task t LEFT OUTER JOIN CLEANSING.dbo.fnt_peopleassignments_potowners pap ON t.id = pap.task_id
WHERE status='Reserved' ORDER BY t.activationtime DESC
SELECT t.id,t.name,t.activationtime,pap.entity_id,pap.task_id,t.status,t.processinstanceid, deploymentid
INTO #listos
FROM CLEANSING.dbo.fnt_task t LEFT OUTER JOIN CLEANSING.dbo.fnt_peopleassignments_potowners pap ON t.id = pap.task_id
WHERE status='Ready' ORDER BY t.activationtime DESC
SELECT t.* INTO #totalAntes
FROM #listos t
WHERE t.processinstanceid NOT IN(SELECT processinstanceid FROM #reservados)

		
DECLARE idTicket CURSOR FOR SELECT fnt.idTicket, fnt.codigoOrigen, fnt.fechaRequerimiento, fnt.processInstanceId
							FROM fnt_cabecera_ticket fnt WHERE fnt.fechaRequerimiento BETWEEN @fechaInicio AND @fechaFin
								AND fnt.codRequerimiento IN(SELECT fntre.id_requerimiento FROM FNT_requerimiento_reload fntre WHERE fntre.id_tipo_requerimiento IN(@IdTipoRequerimiento)) ORDER BY fnt.fechaRequerimiento DESC;
		
		OPEN idTicket
		FETCH NEXT FROM idTicket INTO @ID_TICKET, @CODIGO_ORIGEN, @FECHA_REQUERIMIENTO, @PROCESS_INTANSCE 
			WHILE @@FETCH_STATUS = 0
			BEGIN 

				SELECT TOP 1 @IDENTIFICADOR_TECNICO = CASE WHEN LEN(SUBSTRING(fntmd.valor, (CHARINDEX('Identificador:',fntmd.valor) + (LEN('Identificador:')+1)), 11)) > 0
						THEN SUBSTRING(fntmd.valor, (CHARINDEX('Identificador:',fntmd.valor) + (LEN('Identificador:'))+1), 11) ELSE ' ' END
				FROM fnt_movimiento_cabecera fntmv, fnt_movimiento_detalle fntmd
							WHERE fntmv.idTicket = @ID_TICKET
								AND fntmv.idMovimiento = fntmd.idMovimiento
								AND fntmd.nombre LIKE('Evento')
								AND fntmv.estadoNombre = 'Agendado'
								AND fntmd.idMovimiento IN(fntmv.idMovimiento) ORDER BY fntmv.estadoFechaMovimiento DESC;
				
				SET @FECHA_CONCRETADO = (SELECT TOP 1 fntmv.estadoFechaMovimiento FROM fnt_movimiento_cabecera fntmv WHERE fntmv.idTicket = @ID_TICKET AND fntmv.estadoNombre = 'Completada');
				SELECT @REQUERIMIENTO = tr.descripcion FROM fnt_cabecera_ticket ct
							INNER JOIN fnt_requerimiento_reload rr ON rr.id_requerimiento = ct.codRequerimiento
							INNER JOIN FNT_tipo_requerimiento tr ON tr.id_tipo_requerimiento = rr.id_tipo_requerimiento
							WHERE ct.idTicket = @ID_TICKET;
															
				INSERT INTO #TempRpMasivoCorporativo 
					SELECT  isnull([idTicket], @ID_TICKET),
							isnull([codigoOrigen], @CODIGO_ORIGEN),
							isnull([identificadorTecnico], @IDENTIFICADOR_TECNICO),
							isnull([fechaRegistro], @FECHA_REQUERIMIENTO),
							isnull([diaRegistro], DAY(@FECHA_REQUERIMIENTO)),
							isnull([mesRegistro], MONTH(@FECHA_REQUERIMIENTO)),
							isnull([anioRegistro], YEAR(@FECHA_REQUERIMIENTO)),
							isnull([ultimaMilla],'0'),
							isnull([lineaNegocio], (CASE WHEN (@IdTipoRequerimiento = 35 OR @IdTipoRequerimiento = 34 OR @IdTipoRequerimiento = 33)  THEN 'MASIVA' ELSE 'CORPORATIVO' END )),
							isnull([diaConcretado], DAY(@FECHA_CONCRETADO)),
							isnull([mesConcretado], MONTH(@FECHA_CONCRETADO)),
							isnull([anioConcretado], YEAR(@FECHA_CONCRETADO)),
							isnull([tareaActualBpm], (SELECT a.name FROM DWH.dbo.TBL_TAREAS_FILTRADAS a WHERE a.processinstanceid = @PROCESS_INTANSCE)),
							isnull([requerimiento], @REQUERIMIENTO),
							isnull([fechaPrimeraFactura], (select a.primera_factura from [DWH].dbo.TBL_VIEW_PRIMERA_ULTIMA_FACTURA a where a.codcontrato_plan = @CODIGO_ORIGEN)),
							isnull([Motivo Generacion Visita], (SELECT fntdt.nombre FROM fnt_detalle_ticket fntdt WHERE idTicket =  @ID_TICKET AND (fntdt.nombre =  'Motivo Visita (ADSL)' OR fntdt.nombre =  'Motivo Visita (Wifi)' OR fntdt.nombre =  'Motivo Visita Corporativo' OR fntdt.nombre =  'Motivo Visita(F.O.)')))
								FROM (SELECT nombre, valor FROM fnt_detalle_ticket  WHERE idTicket = @ID_TICKET) AS tab1 
								PIVOT(MAX(valor) FOR nombre IN( [idTicket],
																[codigoOrigen],
																[identificadorTecnico],
																[fechaRegistro],
																[diaRegistro],
																[mesRegistro],
																[anioRegistro],
																[Nombre Tecnico],
																[ultimaMilla],
																[lineaNegocio],
																[diaConcretado],
																[mesConcretado],
																[anioConcretado],
																[tareaActualBpm],
																[requerimiento],
																[fechaPrimeraFactura],
																[Motivo Generacion Visita]))as tab2

			SET @IDENTIFICADOR_TECNICO = ''
			SET @REQUERIMIENTO = ''
			FETCH NEXT FROM idTicket INTO @ID_TICKET, @CODIGO_ORIGEN, @FECHA_REQUERIMIENTO, @PROCESS_INTANSCE
			END
		CLOSE idTicket
		DEALLOCATE idTicket

		Select * from #TempRpMasivoCorporativo;
		--drop table #TempRpMasivoCorporativo;
END
GO


