USE [netplus]
GO

/****** Object:  StoredProcedure [dbo].[usp_rp_masivo_corporativo]    Script Date: 25/5/2022 15:22:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE   PROCEDURE [dbo].[usp_rp_masivo_corporativo]
										@CODIGO_ORIGEN VARCHAR(100),
										@IDENTIFICADOR VARCHAR(100),
										@TIPO_REPORTE VARCHAR(100)
AS
BEGIN
/*
	EXEC usp_rp_masivo_corporativo '1521653', '1719201988', 'Masivo'
	DROP PROCEDURE usp_rp_masivo_corporativo
*/

IF(@CODIGO_ORIGEN is not null)
	BEGIN
		SELECT a.codContrato_Plan AS contratoPlan,
				--a.codCliente AS CODIGO_CLIENTE,
				--a.codContrato AS CODIGO_CONTRATO,
				s.desServicio AS servicio, 
				u.desUltima_Milla AS ultimaMilla,
				c.desCiudad AS ciudadEnlace,
				p.desPlan AS producto,
				tc.desTipo_Contrato AS tipoContrato,
				ec.desEstado_Csp AS estadoContratoPlan,
				vg.des_geografia_oracle AS geografia,
				cli.descripcion AS proyecto,
				(CASE WHEN LEN((SELECT TOP 1 tc.identificador FROM tecnicos_contratista tc WHERE tc.identificador = @IDENTIFICADOR)) > 0 THEN (SELECT TOP 1 tc.organizacion FROM tecnicos_contratista tc WHERE tc.identificador = @IDENTIFICADOR) else @TIPO_REPORTE END) AS planta,
				(SELECT CONCAT(s.codificacion,' || ', s.ubicacion) FROM valor_caracteristica_servicio vcs 
					INNER JOIN splitter s ON vcs.valor = s.codSplitter  WHERE codContratoPlan = @CODIGO_ORIGEN and codCaracteristica = 123) AS splitter,
				(SELECT n.desNodo FROM valor_caracteristica_servicio vcs 
					INNER JOIN nodo n ON vcs.valor = n.codNodo WHERE codContratoPlan = @CODIGO_ORIGEN and codCaracteristica = 53) AS nodo,							
				(SELECT CONCAT(s.codificacion,' || ', s.ubicacion) FROM valor_caracteristica_servicio vcs 
					INNER JOIN splitter s ON vcs.valor = s.codSplitter  WHERE codContratoPlan = @CODIGO_ORIGEN and codCaracteristica = 123) AS cdoe			
			FROM contrato_plan a LEFT JOIN proyecto_cliente cli ON  a.codProyecto = cli.codProyecto
								 INNER JOIN servicio s ON a.codServicio = s.codServicio
								 INNER JOIN ultima_milla u ON a.codUltima_Milla = u.codUltima_Milla
								 INNER JOIN ciudad c ON a.codCiudad = c.codCiudad
								 INNER JOIN planes p ON a.codPlan = p.codPlan
								 INNER JOIN estado_csp ec on a.codEstado_csp = ec.codEstado_Csp
								 INNER JOIN contrato ct ON a.codContrato = ct.codContrato
								 INNER JOIN tipo_contrato tc ON ct.codTipo_Contrato = tc.codTipo_Contrato
								 INNER JOIN view_geografia_costeo_contratoPlan vg ON a.codContrato_Plan = vg.codContratoPlan
				WHERE a.codContrato_Plan = @CODIGO_ORIGEN;
		END
	
END
GO


