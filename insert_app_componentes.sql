DO $$
DECLARE 
	err_code text;
	msg_text text;
	exc_context text;

BEGIN
	BEGIN
		INSERT INTO public.app_componente(
			app_comp_cod, app_comp_nombre, app_comp_descrip)
			VALUES ('WS-SOACS', 'Web Service Soacs API', 'Consumo de api SOACS, para obtener estados');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'get.order.ps', 'https://soacsdev.puntonet.ec:9174/osb/order/ofsc/consult/', null, 'Consulta de informacion de la orden en servicio de SOAP.', 'A', '2022-05-27');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'pass.consult.resource', 'd0aa7c7f6ceed2711f6f667a333699f5cf222504e10d67e35049be590037497d', null, 'Clave para consultar la informacion del técnico de FIELD.', 'A', '2022-05-27');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'pass.get.order.ps', 'A*4ut3RDj1Huxaj#1', null, 'Clave para consultar la informacion de la orden en servicio de SOAP.', 'A', '2022-05-27');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'url.consult.resource', 'https://puntonet.etadirect.com/rest/ofscCore/v1/resources/', null, 'Url para consulta de informacion sobre el técnico de FIELD.', 'A', '2022-05-27');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'usuario.consult.resource', 'client_rest_api@puntonet', null, 'Usuario para consultar la informacion del técnico de FIELD.', 'A', '2022-05-27');

		INSERT INTO public.app_componente_config(
			app_comp_cod, app_comp_config_cod, app_comp_config_valor, app_comp_config_valor2, app_comp_config_descrip, app_comp_config_estado, app_comp_config_fecha_crea)
			VALUES ('WS-SOACS', 'usuario.get.order.ps', 'integration', null, 'Usuario para consultar la informacion de la orden en servicio de SOAP.', 'A', '2022-05-27');
	EXCEPTION WHEN OTHERS THEN
		delete from public.app_componente where app_comp_cod = 'WS-SOACS' AND app_comp_nombre = 'Web Service Soacs API';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'get.order.ps';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'pass.consult.resource';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'pass.get.order.ps';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'url.consult.resource';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'usuario.consult.resource';
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'usuario.get.order.ps';
		
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error: % || Causa: % || Contenido: % || Accion: Se termina el reverso.', 
			err_code, msg_text, exc_context;
	END;
END;
$$;