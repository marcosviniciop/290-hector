DO $$
DECLARE 
	err_code text;
	msg_text text;
	exc_context text;

BEGIN
	BEGIN
		delete from public.app_componente where app_comp_cod = 'WS-SOACS' AND app_comp_nombre = 'Web Service Soacs API';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_1: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'get.order.ps';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_2: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'pass.consult.resource';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_3: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'pass.get.order.ps';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_4: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'url.consult.resource';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_5: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'usuario.consult.resource';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_6: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
	
	BEGIN
		delete from public.app_componente_config where app_comp_cod = 'WS-SOACS' AND app_comp_config_cod = 'usuario.get.order.ps';
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error_7: % || Causa: % || Contenido: %', 
			err_code, msg_text, exc_context;
	END;
END;
$$;
