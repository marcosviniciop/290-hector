DO $$
DECLARE 
	err_code text;
	msg_text text;
	exc_context text;

BEGIN
	BEGIN
		INSERT INTO requerimiento.menu_principal (
			sitema_id, nombre, id_estado_menu, url, accion_url, icono, id_menu_padre, orden) 
			VALUES(2, 'Reporte Masivo y Corporativo', 1, 1, '/faces/paginas/requerimientos/reporte/reportesMasivoCorporativo.xhtml',NULL, 83,4);
	EXCEPTION WHEN OTHERS THEN
		delete from requerimiento.menu_principal where sitema_id = 2 AND nombre = 'Reporte Masivo y Corporativo' AND id_estado_menu = 1 AND url = 1
			AND accion_url = '/faces/paginas/requerimientos/reporte/reportesMasivoCorporativo.xhtml' AND id_menu_padre = 83 AND orden = 4;
		
		GET STACKED DIAGNOSTICS
			err_code = RETURNED_SQLSTATE,
			msg_text = MESSAGE_TEXT,
			exc_context = PG_CONTEXT;

			RAISE NOTICE 'Codigo de Error: % || Causa: % || Contenido: % || Accion: Se termina el reverso.', 
			err_code, msg_text, exc_context;
	END;
END;
$$;


